package com.techmojo.core.exception;

@SuppressWarnings("serial")
public class ServiceException extends RuntimeException {

	protected String errorCode;

	public ServiceException() {
		super();
	}
	
	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(String message, String errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public ServiceException(Throwable throwable) {
		super(throwable);
	}
	
	public String getErrorCode() {
		return errorCode;
	}
}

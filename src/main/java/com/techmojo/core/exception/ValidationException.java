package com.techmojo.core.exception;

@SuppressWarnings("serial")
public class ValidationException extends ServiceException {

	private String[] messages;

	public ValidationException() {
		super();
		this.messages = null;
	}

	public ValidationException(String message) {
		super();
		this.messages = new String[] { message };
	}

	public ValidationException(String[] messages) {
		super();
		this.messages = messages;
	}
	
	public ValidationException(String[] messages, String errorCode) {
		super();
		this.messages = messages;
		this.errorCode = errorCode;
	}

	public ValidationException(String message, String errorCode) {
		super(message, errorCode);
		this.messages = new String[] { message };
	}

	public String[] getMessages() {
		return messages;
	}

}

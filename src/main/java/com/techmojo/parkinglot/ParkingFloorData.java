package com.techmojo.parkinglot;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.techmojo.parkinglot.vehicle.VehicleType;

@JsonInclude(value = Include.NON_EMPTY)
public class ParkingFloorData implements Comparable<ParkingFloorData> {

	private String id;
	private String name;
	private final Integer level;
	private final Map<VehicleType, List<ParkingSlot>> availableParkingSlots;

	public ParkingFloorData(String id, String name, Integer level,
			Map<VehicleType, List<ParkingSlot>> availableParkingSlots) {
		super();
		this.id = id;
		this.name = name;
		this.level = level;
		this.availableParkingSlots = availableParkingSlots;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLevel() {
		return level;
	}

	public boolean isFull() {
		Set<Entry<VehicleType, List<ParkingSlot>>> availableParkingSlotsEntrySet = availableParkingSlots.entrySet();
		boolean isFull = false;
		for (Entry<VehicleType, List<ParkingSlot>> availableParkingSlotsEntry : availableParkingSlotsEntrySet) {
			isFull = !availableParkingSlotsEntry.getValue().isEmpty();
			if (!isFull) {
				break;
			}
		}
		return isFull;
	}

	public boolean isParkingSlotAvailableVehicleType(VehicleType vehicleType) {
		return !availableParkingSlots.get(vehicleType).isEmpty();
	}

	public ParkingSlot getParkingSlotForVehicleType(VehicleType vehicleType) {
		if (isParkingSlotAvailableVehicleType(vehicleType)) {
			return availableParkingSlots.get(vehicleType).remove(0);
		}
		return null;
	}

	public void markParkingSlotAsAviable(ParkingSlot parkingSlot) {
		availableParkingSlots.get(parkingSlot.getVehicleType()).add(parkingSlot);
	}

	@Override
	public int compareTo(ParkingFloorData other) {
		return this.getLevel().compareTo(other.getLevel());
	}

}
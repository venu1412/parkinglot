package com.techmojo.parkinglot.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.techmojo.core.exception.ServiceException;
import com.techmojo.parkinglot.ParkingFloorData;
import com.techmojo.parkinglot.ParkingSlot;
import com.techmojo.parkinglot.vehicle.VehicleType;

@Component
public class ParkingLotManagementDataService {

	private String id;
	private String name;
	private boolean parkingClosed;
	private List<ParkingFloorData> parkingFloors;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ParkingFloorData> getParkingFloors() {
		return parkingFloors;
	}

	public void setParkingFloors(List<ParkingFloorData> parkingFloors) {
		this.parkingFloors = parkingFloors;
	}

	public boolean isParkingClosed() {
		return parkingClosed;
	}

	public void setParkingClosed(boolean closed) {
		this.parkingClosed = closed;
	}

	public ParkingSlot getParkingSpotForVehicleType(VehicleType vehicleType) {
		for (ParkingFloorData parkingFloorData : parkingFloors) {
			if (parkingFloorData.isParkingSlotAvailableVehicleType(vehicleType)) {
				ParkingSlot parkingSlotForVehicleType = parkingFloorData.getParkingSlotForVehicleType(vehicleType);
				return parkingSlotForVehicleType;
			}
		}
		throw new ServiceException("No parking spot available for vehicleType: " + vehicleType);
	}

	public void markParkingSlotAsAvailable(ParkingSlot parkingSlot) {
		String floorId = parkingSlot.getFloorId();
		for (ParkingFloorData parkingFloorData : parkingFloors) {
			if (parkingFloorData.getId().equals(floorId)) {
				parkingFloorData.markParkingSlotAsAviable(parkingSlot);
				return;
			}
		}
		throw new ServiceException(
				"Invalid floor Id: " + parkingSlot.getFloorId() + " for parking spot: " + parkingSlot.getId());
	}

	@PostConstruct
	private void init() {
		parkingClosed = false;
		parkingFloors = new ArrayList<>();
		// This should be loaded from database
		ParkingFloorData zeroFloor = createParkingFloorData("1", "Basement", 0, 2, 3, 1);
		ParkingFloorData firstFloor = createParkingFloorData("2", "First Floor", 1, 5, 1, 1);
		parkingFloors.add(zeroFloor);
		parkingFloors.add(firstFloor);
	}

	public ParkingFloorData createParkingFloorData(String floorId, String floorName, int floorLevel,
			int availableBikeSlots, int availableCarSlots, int availableBusSlots) {
		Map<VehicleType, List<ParkingSlot>> parkingSlotsData = new HashMap<>();
		parkingSlotsData.put(VehicleType.BIKE,
				createParkingSlots(VehicleType.BIKE, availableBikeSlots, floorName, floorId));
		parkingSlotsData.put(VehicleType.CAR,
				createParkingSlots(VehicleType.CAR, availableCarSlots, floorName, floorId));
		parkingSlotsData.put(VehicleType.BUS,
				createParkingSlots(VehicleType.BUS, availableBusSlots, floorName, floorId));
		ParkingFloorData zeroFloor = new ParkingFloorData(floorId, floorName, floorLevel, parkingSlotsData);
		return zeroFloor;
	}

	private List<ParkingSlot> createParkingSlots(VehicleType vehicleType, int spotsCount, String floorName,
			String floorId) {
		List<ParkingSlot> parkingSlots = new ArrayList<>();
		for (int index = 0; index < spotsCount; index++) {
			ParkingSlot parkingSlot = new ParkingSlot();
			parkingSlot.setVehicleType(vehicleType);
			parkingSlot.setFloorId(floorId);
			parkingSlot.setName(vehicleType.getCode() + "-" + floorName + "-" + index);
			parkingSlots.add(parkingSlot);
		}
		return parkingSlots;
	}

}

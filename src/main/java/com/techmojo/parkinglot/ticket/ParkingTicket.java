package com.techmojo.parkinglot.ticket;

import java.math.BigDecimal;
import java.util.Date;

import org.assertj.core.annotations.NonNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.techmojo.parkinglot.ParkingSlot;
import com.techmojo.parkinglot.vehicle.Vehicle;

@JsonInclude(value = Include.NON_EMPTY)
public class ParkingTicket {

	private String id;
	@NonNull
	private final Vehicle vehicle;
	@NonNull
	private Date checkInTime;
	private ParkingSlot parkingSlot;
	private Date checkOutTime;
	private BigDecimal parkingFee;

	public ParkingTicket(Vehicle vehicle) {
		super();
		this.vehicle = vehicle;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public Date getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(Date checkInTime) {
		this.checkInTime = checkInTime;
	}

	public Date getCheckOutTime() {
		return checkOutTime;
	}

	public void setCheckOutTime(Date checkOutTime) {
		this.checkOutTime = checkOutTime;
	}

	public ParkingSlot getParkingSlot() {
		return parkingSlot;
	}

	public void setParkingSlot(ParkingSlot parkingSlot) {
		this.parkingSlot = parkingSlot;
	}

	public BigDecimal getParkingFee() {
		return parkingFee;
	}

	public void setParkingFee(BigDecimal parkingFee) {
		this.parkingFee = parkingFee;
	}

	public String getParkingFeeFormated() {
		if (this.parkingFee != null) {
			return "Rs." + parkingFee;
		}
		return null;
	}

}

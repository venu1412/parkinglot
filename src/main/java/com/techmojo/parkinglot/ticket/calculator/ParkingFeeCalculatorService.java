package com.techmojo.parkinglot.ticket.calculator;

import java.math.BigDecimal;

import com.techmojo.parkinglot.ticket.ParkingTicket;

public interface ParkingFeeCalculatorService {

	BigDecimal calculateParkingFee(ParkingTicket parkingTicket);

}

package com.techmojo.parkinglot.ticket.calculator;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.techmojo.parkinglot.ticket.ParkingTicket;
import com.techmojo.parkinglot.vehicle.VehicleType;

@Component
public class ParkingFeeCalculatorServiceImpl implements ParkingFeeCalculatorService {

	private Map<VehicleType, BigDecimal> parkingFeePerHour;

	@Override
	public BigDecimal calculateParkingFee(ParkingTicket parkingTicket) {
		return parkingFeePerHour.get(parkingTicket.getVehicle().getVehicleType())
				.multiply(getParkingTimeInHours(parkingTicket));
	}

	public BigDecimal getParkingTimeInHours(ParkingTicket parkingTicket) {
		long parkingEndTime = parkingTicket.getCheckOutTime().getTime();
		long parkingStartTime = parkingTicket.getCheckInTime().getTime();
		long parkingTimeInMilliSeconds = parkingEndTime - parkingStartTime;
		return new BigDecimal(Math.ceil(convertTimeToHours(parkingTimeInMilliSeconds)));
	}

	// If parking Time is 61 Minutes it calculates as 2 hours, This should be
	// customized based on the customer needs
	public double convertTimeToHours(long parkingTimeInMilliSeconds) {
		if (parkingTimeInMilliSeconds == 0) {
			return 1;
		}
		double numberOfMilliSecondsPerHour = 1000.0 * 60 * 60;
		return parkingTimeInMilliSeconds / numberOfMilliSecondsPerHour;
	}

	@PostConstruct
	private void init() {
		/*
		 * Initialize this from Database/Configuration
		 */
		parkingFeePerHour = new HashMap<>();
		parkingFeePerHour.put(VehicleType.BIKE, new BigDecimal("10"));
		parkingFeePerHour.put(VehicleType.CAR, new BigDecimal("30"));
		parkingFeePerHour.put(VehicleType.BUS, new BigDecimal("100"));
	}

	// public static void main(String[] args) throws InterruptedException {
	// ParkingFeeCalculatorImpl parkingFeeCalculatorImpl = new
	// ParkingFeeCalculatorImpl();
	// parkingFeeCalculatorImpl.init();
	// ParkingTicket parkingTicket = new ParkingTicket();
	// Vehicle vehicle = new Vehicle();
	// vehicle.setVehicleType(VehicleType.CAR);
	// parkingTicket.setVehicle(vehicle);
	// parkingTicket.setCheckInTime(new DateTime().minusMinutes(121).toDate());
	// parkingTicket.setCheckOutTime(new Date());
	// System.out.println(parkingFeeCalculatorImpl.getParkingTimeInHours(parkingTicket));
	// BigDecimal parkingFee =
	// parkingFeeCalculatorImpl.calculateParkingFee(parkingTicket);
	// System.out.println(parkingFee);
	// }

}

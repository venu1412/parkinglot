package com.techmojo.parkinglot.ticket.service;

import com.techmojo.parkinglot.ticket.ParkingTicket;
import com.techmojo.parkinglot.vehicle.Vehicle;

public interface ParkingTicketService {

	ParkingTicket checkInVehicle(Vehicle vehicle);

	ParkingTicket checkOutVehicle(String vehicleNumber);

}

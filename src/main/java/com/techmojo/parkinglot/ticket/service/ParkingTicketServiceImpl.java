package com.techmojo.parkinglot.ticket.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techmojo.core.exception.ServiceException;
import com.techmojo.core.exception.ValidationException;
import com.techmojo.parkinglot.ParkingSlot;
import com.techmojo.parkinglot.service.ParkingLotManagementDataService;
import com.techmojo.parkinglot.ticket.ParkingTicket;
import com.techmojo.parkinglot.ticket.calculator.ParkingFeeCalculatorService;
import com.techmojo.parkinglot.vehicle.Vehicle;
import com.techmojo.util.model.validator.ModelValidator;

@Service
public class ParkingTicketServiceImpl implements ParkingTicketService {

	@Autowired
	private ParkingLotManagementDataService parkingLotManagementDataService;

	@Autowired
	private ModelValidator modelValidator;

	@Autowired
	private ParkingFeeCalculatorService parkingFeeCalculatorService;

	
	// These should be fetched from DB, or lazily initialized
	private Map<String, ParkingTicket> parkingTicketsGiven;

	@Override
	public synchronized ParkingTicket checkInVehicle(Vehicle vehicle) {
		ParkingTicket parkingTicket = new ParkingTicket(vehicle);
		setDefaultDataForParkingTicketCreate(parkingTicket);
		validateParkingTicketCreate(parkingTicket);
		ParkingSlot parkingSpotForVehicleType = parkingLotManagementDataService
				.getParkingSpotForVehicleType(vehicle.getVehicleType());
		if (parkingSpotForVehicleType != null) {
			parkingTicket.setParkingSlot(parkingSpotForVehicleType);
			parkingTicket.setCheckInTime(new Date());
			// Store This In DB
			parkingTicketsGiven.put(parkingTicket.getVehicle().getVehicleNumber(), parkingTicket);
		}
		return parkingTicket;
	}

	@Override
	public synchronized ParkingTicket checkOutVehicle(String vehicleNumber) {
		modelValidator.validateBlank("Vehicle Number", vehicleNumber);
		ParkingTicket parkingTicket = parkingTicketsGiven.get(vehicleNumber);
		if (parkingTicket == null) {
			throw new ServiceException("Invalid Vehicle Number: " + vehicleNumber + ", No parking alloted for vehicle");
		}
		parkingTicket.setCheckOutTime(new Date());
		parkingLotManagementDataService.markParkingSlotAsAvailable(parkingTicket.getParkingSlot());
		BigDecimal parkingFee = parkingFeeCalculatorService.calculateParkingFee(parkingTicket);
		parkingTicket.setParkingFee(parkingFee);
		return parkingTicket;
	}

	public void setDefaultDataForParkingTicketCreate(ParkingTicket parkingTicket) {
		parkingTicket.setCheckOutTime(null);
	}

	public void validateParkingTicketCreate(ParkingTicket parkingTicket) {
		modelValidator.validateNull("Vehicle", parkingTicket.getVehicle());
		modelValidator.validate(parkingTicket);
		modelValidator.validate(parkingTicket.getVehicle());
		if (parkingTicketsGiven.get(parkingTicket.getVehicle().getVehicleNumber()) != null) {
			throw new ValidationException("Active Parking Ticket already exist for vehicle: "
					+ parkingTicket.getVehicle().getVehicleNumber());
		}
	}

	@PostConstruct
	private void init() {
		// This fetch from DB, or look out in DB lazily.
		parkingTicketsGiven = new HashMap<>();
	}

}

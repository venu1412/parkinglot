package com.techmojo.parkinglot.vehicle;

public enum VehicleType {

	BIKE("bike"), CAR("car"), BUS("bus");

	private String code;

	private VehicleType(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

}

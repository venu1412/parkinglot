package com.techmojo.parkinglot.web.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techmojo.parkinglot.service.ParkingLotManagementDataService;

@RestController
public class AdminController extends AbstractRestBaseController {

	@Autowired
	private ParkingLotManagementDataService parkingLotManagementDataService;

	@PutMapping("/admin/parkingLot/open")
	public void openParking() {
		parkingLotManagementDataService.setParkingClosed(false);
	}

	@PutMapping("/admin/parkingLot/closed")
	public void closeParking() {
		parkingLotManagementDataService.setParkingClosed(true);
	}

}

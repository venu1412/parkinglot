package com.techmojo.parkinglot.web.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.techmojo.parkinglot.ticket.ParkingTicket;
import com.techmojo.parkinglot.ticket.service.ParkingTicketService;
import com.techmojo.parkinglot.vehicle.Vehicle;

@RestController
public class ParkingLotRestController extends AbstractRestBaseController {

	@Autowired
	private ParkingTicketService parkingTicketService;

	@PostMapping(path = "/parking/checkIn")
	public ParkingTicket getParking(@RequestBody Vehicle vehicle) {
		return parkingTicketService.checkInVehicle(vehicle);
	}

	@PostMapping(path = "/parking/checkOut")
	public ParkingTicket checkOutVehicle(@RequestBody Vehicle vehicle) {
		return parkingTicketService.checkOutVehicle(vehicle.getVehicleNumber());
	}

}
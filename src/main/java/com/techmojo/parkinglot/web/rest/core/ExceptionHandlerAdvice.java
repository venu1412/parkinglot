package com.techmojo.parkinglot.web.rest.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.techmojo.core.exception.ServiceException;
import com.techmojo.core.exception.ValidationException;

@ControllerAdvice
public class ExceptionHandlerAdvice {

	@ExceptionHandler({ ValidationException.class })
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public @ResponseBody ErrorMessage handleValidationException(ValidationException ex) {
		return new ErrorMessage(ex.getErrorCode(), ex.getMessages());
	}

	@ExceptionHandler({ ServiceException.class })
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ErrorMessage handleServiceException(ServiceException ex) {
		return new ErrorMessage(ex.getErrorCode(), ex.getMessage());
	}

	@JsonInclude(value = Include.NON_EMPTY)
	public static class ErrorMessage {

		private List<String> messages;
		private String errorCode;

		public ErrorMessage() {
			super();
		}

		private ErrorMessage(String errorCode, final String... messages) {
			this.errorCode = errorCode;
			if (messages != null && messages.length > 0) {
				this.messages = new ArrayList<String>();
				this.messages.addAll(Arrays.asList(messages));
			}
		}

		public List<String> getMessages() {
			return messages;
		}

		public void setMessages(List<String> messages) {
			this.messages = messages;
		}

		public String getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}

	}
}

package com.techmojo.util.model.validator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.hibernate.validator.HibernateValidator;
import org.springframework.stereotype.Service;

import com.techmojo.core.exception.ValidationException;

@Service
public class HibernateModelValidator implements ModelValidator {

	private Validator validator;

	public HibernateModelValidator() {
		ValidatorFactory factory = Validation.byProvider(HibernateValidator.class).configure().buildValidatorFactory();
		this.validator = factory.getValidator();
	}

	@Override
	public <T> void validate(T model) throws ValidationException {
		if (model == null) {
			throw new ValidationException(new String[] { "Model should not be null" });
		}
		// validate
		Set<ConstraintViolation<T>> constraints = this.validator.validate(model);
		// check constraints violation
		checkConstrainsts(constraints);
	}

	@Override
	public <T> void validate(T model, String... propertyNames) throws ValidationException {
		if (model == null || propertyNames == null || propertyNames.length == 0) {
			throw new ValidationException(new String[] { "Model or properties should not be null" });
		}
		Set<ConstraintViolation<T>> constraints = new HashSet<ConstraintViolation<T>>();
		for (String property : propertyNames) {
			Set<ConstraintViolation<T>> results = this.validator.validateProperty(model, property);
			constraints.addAll(results);
		}
		// check constraints violation
		checkConstrainsts(constraints);
	}

	@Override
	public void validateBlank(String propertyName, String value) throws ValidationException {
		if (value == null || value.trim().isEmpty()) {
			String message = propertyName + " is blank";
			throw new ValidationException(new String[] { message });
		}
	}

	@Override
	public void validateNull(String propertyName, Object value) throws ValidationException {
		if (value == null) {
			String message = propertyName + " is empty";
			throw new ValidationException(new String[] { message });
		}
	}

	private <T> String[] resolveMessages(Set<ConstraintViolation<T>> constraints) {
		List<String> messages = new ArrayList<String>();
		for (ConstraintViolation<T> constraint : constraints) {
			String message = resolveMessage(constraint);
			messages.add(message);
		}
		return messages.toArray(new String[messages.size()]);
	}

	private <T> String resolveMessage(ConstraintViolation<T> constraint) {
		return constraint.getPropertyPath().toString().concat(" ").concat(constraint.getMessage());
	}

	private <T> void checkConstrainsts(Set<ConstraintViolation<T>> constraints) {
		if (!constraints.isEmpty()) {
			throw new ValidationException(resolveMessages(constraints));
		}
	}

}

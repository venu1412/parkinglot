package com.techmojo.util.model.validator;

import com.techmojo.core.exception.ValidationException;

public interface ModelValidator {

	public <T> void validate(T model) throws ValidationException;

	public <T> void validate(T model, String... propertyNames) throws ValidationException;

	public void validateBlank(String propertyName, String value) throws ValidationException;

	public void validateNull(String propertyName, Object value) throws ValidationException;

}
